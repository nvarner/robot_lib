use crate::kalman_filter::{ControlInputMatrixCreator, KalmanFilter};
use crate::state_estimate::StateEstimate;
use crate::units::{Angle, AngularVelocity, Length, Velocity};
use nalgebra::allocator::Allocator;
use nalgebra::constraint::{AreMultipliable, ShapeConstraint};
use nalgebra::{
    DefaultAllocator, Matrix4x3, Matrix6, MatrixMN, RowVector3, RowVector4, Vector2, Vector3,
    Vector4, Vector6, U1, U4, U6,
};
use uom::si::angle::radian;
use uom::si::angular_velocity::radian_per_second;
use uom::si::length::decimeter;
use uom::si::velocity::decimeter_per_second;

// TODO: Convert dt to use units

pub struct Robot<'a, W: 'a + ControlInputMatrixCreator<f64, U6>>
where
    ShapeConstraint: AreMultipliable<U6, W::C, W::C, U1>,
    DefaultAllocator: Allocator<f64, U6, W::C> + Allocator<f64, W::C, U1>,
{
    kalman_filter: KalmanFilter<'a, f64, U6, W>,
}

impl<'a, W: 'a + ControlInputMatrixCreator<f64, U6, C = U4>> Robot<'a, W>
where
    ShapeConstraint: AreMultipliable<U6, W::C, W::C, U1>,
    DefaultAllocator: Allocator<f64, U6, W::C> + Allocator<f64, W::C, U1>,
{
    pub fn new(
        initial_pose: PoseEstimate,
        wheel_setup: W,
        process_noise_covariance: Matrix6<f64>,
    ) -> Self {
        let kalman_filter = KalmanFilter::new(
            initial_pose,
            &Self::create_state_transition_matrix,
            wheel_setup,
            process_noise_covariance,
        );

        Self { kalman_filter }
    }

    pub fn update_with_sensor_readings(&mut self, readings: Vec<&PoseEstimate>, dt: f64) {
        self.kalman_filter
            .predict(&Vector4::new(0.0, 0.0, 0.0, 0.0), dt);

        for reading in readings {
            self.kalman_filter.update(reading);
        }
    }

    fn create_state_transition_matrix(dt: f64) -> MatrixMN<f64, U6, U6> {
        MatrixMN::from_row_slice_generic(
            U6,
            U6,
            &[
                1.0, dt, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, dt, 0.0,
                0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, dt, 0.0, 0.0, 0.0, 0.0,
                0.0, 1.0,
            ],
        )
    }
}

pub type PoseEstimate = StateEstimate<f64, U6>;

impl PoseEstimate {
    pub fn new_stationary(
        position: Vector2<Length>,
        position_variance: Vector2<f64>,
        angle: Angle,
        angle_variance: f64,
    ) -> Self {
        // *  x  y  vx vy ...
        // x  .  .  .  .
        // y  .  .  .  .
        // vx .  .  .  .
        // vy .  .  .  .
        // ...
        // x, y, and angle have their own variances, nothing else affects them or anything else
        // because velocity and acceleration are known to be zero with zero variance
        let covariance_matrix = MatrixMN::from_columns(&[
            Vector6::new(position_variance.x, 0.0, 0.0, 0.0, 0.0, 0.0),
            Vector6::new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            Vector6::new(0.0, position_variance.y, 0.0, 0.0, 0.0, 0.0),
            Vector6::new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
            Vector6::new(0.0, 0.0, 0.0, 0.0, angle_variance, 0.0),
            Vector6::new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0),
        ]);

        Self::new(
            position,
            Vector2::new(
                Velocity::new::<decimeter_per_second>(0.0),
                Velocity::new::<decimeter_per_second>(0.0),
            ),
            angle,
            AngularVelocity::new::<radian_per_second>(0.0),
            covariance_matrix,
        )
    }

    pub fn new(
        position: Vector2<Length>,
        velocity: Vector2<Velocity>,
        angle: Angle,
        angular_velocity: AngularVelocity,
        covariance_matrix: MatrixMN<f64, U6, U6>,
    ) -> Self {
        let mean = Vector6::new(
            position.x.get::<decimeter>(),
            velocity.x.get::<decimeter_per_second>(),
            position.y.get::<decimeter>(),
            velocity.y.get::<decimeter_per_second>(),
            angle.get::<radian>(),
            angular_velocity.get::<radian_per_second>(),
        );

        Self {
            mean,
            covariance: covariance_matrix,
        }
    }
}

pub struct OmniWheelSetup {
    half_r: f64,
    neg_half_r: f64,
    r_over_4l: f64,
    r_over_4w: f64,
    wheel_speed_matrix: Matrix4x3<f64>,
}

impl OmniWheelSetup {
    // TODO: Convert these to use units
    pub fn new(half_width: f64, half_length: f64, wheel_rad: f64) -> Self {
        let half_r = wheel_rad / 2.0;
        let neg_half_r = -half_r;
        let r_over_4l = wheel_rad / (4.0 * half_length);
        let r_over_4w = wheel_rad / (4.0 * half_width);

        let wheel_speed_matrix = Matrix4x3::from_rows(&[
            RowVector3::new(-1.0 / wheel_rad, 0.0, half_length / wheel_rad),
            RowVector3::new(1.0 / wheel_rad, 0.0, half_length / wheel_rad),
            RowVector3::new(0.0, 1.0 / wheel_rad, half_width / wheel_rad),
            RowVector3::new(0.0, -1.0 / wheel_rad, half_width / wheel_rad),
        ]);

        Self {
            half_r,
            neg_half_r,
            r_over_4l,
            r_over_4w,
            wheel_speed_matrix,
        }
    }

    pub fn calculate_wheel_speeds(
        &self,
        velocity: &Vector2<f64>,
        angular_velocity: f64,
    ) -> Vector4<f64> {
        let kinematics_vector = Vector3::new(velocity[0], velocity[1], angular_velocity);
        &self.wheel_speed_matrix * kinematics_vector
    }
}

impl ControlInputMatrixCreator<f64, U6> for OmniWheelSetup {
    type C = U4;

    fn create_control_input_matrix(&self, dt: f64) -> MatrixMN<f64, U6, U4> {
        MatrixMN::from_rows(&[
            RowVector4::new(self.neg_half_r * dt, self.half_r * dt, 0.0, 0.0),
            RowVector4::new(self.neg_half_r, self.half_r, 0.0, 0.0),
            RowVector4::new(0.0, 0.0, self.half_r * dt, self.neg_half_r * dt),
            RowVector4::new(0.0, 0.0, self.half_r, self.neg_half_r),
            RowVector4::new(
                self.r_over_4l * dt,
                self.r_over_4l * dt,
                self.r_over_4w * dt,
                self.r_over_4w * dt,
            ),
            RowVector4::new(
                self.r_over_4l,
                self.r_over_4l,
                self.r_over_4w,
                self.r_over_4w,
            ),
        ])
    }
}
