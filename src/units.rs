use uom::si::acceleration::Acceleration as A;
use uom::si::angle::Angle as An;
use uom::si::angular_acceleration::AngularAcceleration as AnA;
use uom::si::angular_velocity::AngularVelocity as AnV;
use uom::si::length::Length as L;
use uom::si::velocity::Velocity as V;
use uom::si::SI;

pub type Length = L<SI<f64>, f64>;
pub type Velocity = V<SI<f64>, f64>;
pub type Acceleration = A<SI<f64>, f64>;
pub type Angle = An<SI<f64>, f64>;
pub type AngularVelocity = AnV<SI<f64>, f64>;
pub type AngularAcceleration = AnA<SI<f64>, f64>;
