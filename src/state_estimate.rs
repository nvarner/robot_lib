use nalgebra::base::allocator::Allocator;
use nalgebra::base::constraint::AreMultipliable;
use nalgebra::constraint::ShapeConstraint;
use nalgebra::{DefaultAllocator, Dim, DimName, MatrixMN, RealField, VectorN, U1};

#[derive(Debug)]
/// An estimate of some state. The estimate is not actually a single estimate, but an
/// `S`-dimensional normal distribution of estimates.
///
/// Put more simply, the estimate is just the one most likely state (the mean) and the information
/// needed to figure out exactly how likely any state is based on where it is relative to the mean.
///
/// States closer to the mean are more likely to be the one true state while states farther from the
/// mean are less likely to be the one true state.
///
/// The covariance matrix is a table that describes how each variable in the state correlates to
/// every other variable in the state. It's what's used to determine how likely any state is. The
/// covariance matrix can be set up so that any state that's close to the mean has the same chance
/// of being the one true state (making a circular shaped graph), or make it depend on if it's
/// above/below and left/right of the mean (making an elliptical shaped graph).
///
/// # Type variables
///  - `T`: The type that the state vector holds. `f64` is recommended in most cases.
///  - `S`: The size of the state vector.
pub struct StateEstimate<T: RealField, S: Dim + DimName>
where
    ShapeConstraint: AreMultipliable<S, S, S, S>,
    DefaultAllocator: Allocator<T, S, S> + Allocator<T, S, U1>,
{
    pub mean: VectorN<T, S>,
    pub covariance: MatrixMN<T, S, S>,
}
