use crate::state_estimate::StateEstimate;
use nalgebra::allocator::Allocator;
use nalgebra::constraint::{AreMultipliable, ShapeConstraint};
use nalgebra::{DefaultAllocator, Dim, DimName, MatrixMN, RealField, VectorN, U1};

/// Struct representing a Kalman filter
///
/// # Type parameters
///  - `T`: A number type that will be used to store all numbers. `f64` is recommended.
///  - `S`: The size of the state to track
///  - `C`: The size of the control inputs
pub struct KalmanFilter<'a, T: RealField, S: Dim + DimName, M: ControlInputMatrixCreator<T, S>>
where
    ShapeConstraint: AreMultipliable<S, S, S, S>,
    ShapeConstraint: AreMultipliable<S, M::C, M::C, U1>,
    DefaultAllocator:
        Allocator<T, S, S> + Allocator<T, S, M::C> + Allocator<T, S, U1> + Allocator<T, M::C, U1>,
{
    /// Current predicted state
    x: StateEstimate<T, S>,
    /// Function returning the matrix that transitions from one time step to the next, sometimes
    /// called F, given the time since the last update
    f: &'a dyn Fn(T) -> MatrixMN<T, S, S>,
    /// Function returning the matrix that accounts for known control inputs, sometimes called B,
    /// given the time since the last update
    b: M,
    /// Process noise covariance, or the covariance matrix that represents how uncertain we are in
    /// our prediction because of external factors outside of our control, like wind, sometimes
    /// called Q
    q: MatrixMN<T, S, S>,
}

impl<'a, T: RealField, S: Dim + DimName, M: ControlInputMatrixCreator<T, S>>
    KalmanFilter<'a, T, S, M>
where
    ShapeConstraint: AreMultipliable<S, S, S, S>,
    ShapeConstraint: AreMultipliable<S, M::C, M::C, U1>,
    DefaultAllocator:
        Allocator<T, S, S> + Allocator<T, S, M::C> + Allocator<T, S, U1> + Allocator<T, M::C, U1>,
{
    pub fn new(
        x: StateEstimate<T, S>,
        f: &'a dyn Fn(T) -> MatrixMN<T, S, S>,
        b: M,
        q: MatrixMN<T, S, S>,
    ) -> Self {
        Self { x, f, b, q }
    }

    /// Predict the current state estimate for the next time step.
    ///
    /// # Parameters:
    ///  - `u`: Control vector. Combined with the control matrix to update predictions based on
    /// commands sent to the plant.
    pub fn predict(&mut self, u: &VectorN<T, M::C>, dt: T) {
        let state_transition = &(self.f)(dt);
        self.x.mean =
            (state_transition * &self.x.mean) + (self.b.create_control_input_matrix(dt) * u);
        self.x.covariance =
            state_transition * &self.x.covariance * state_transition.transpose() + &self.q;
    }

    /// Update the prediction based on sensor readings.
    ///
    /// # Parameters
    ///  - `reading`: The reading taken by the sensor.
    pub fn update(&mut self, reading: &StateEstimate<T, S>) {
        let k = self.kalman_gain(reading);
        self.x.mean += &k * (&reading.mean - &self.x.mean);
        self.x.covariance -= &k * &self.x.covariance;
    }

    fn kalman_gain(&self, reading: &StateEstimate<T, S>) -> MatrixMN<T, S, S> {
        let inv_part = (&self.x.covariance + &reading.covariance)
            .try_inverse()
            .expect("Should be able to find inverse in Kalman gain matrix");
        &self.x.covariance * inv_part
    }
}

pub trait ControlInputMatrixCreator<T: RealField, S: Dim + DimName>
where
    DefaultAllocator: Allocator<T, S, Self::C>,
{
    type C: Dim + DimName;

    fn create_control_input_matrix(&self, dt: T) -> MatrixMN<T, S, Self::C>;
}
